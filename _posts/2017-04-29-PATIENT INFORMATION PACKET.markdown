---
layout: post
title:  "Patient Information Packet"
date:   2017-04-29 18:26:57 -0500
categories: 
permalink: /patient-information-packet/
---
<p>Our Patient Information Packet has been compiled as a resource to provide our patients current information about our clinic policies and helpful visit information.</p>

<p>Click on the links below for important information regarding your care at AALFA Family Clinic. If you have any questions, feel free to contact our Office Manager at 651-653-0062.</p>

<a href="/pdf/Patient-Welcome-Letter.pdf">Patient Welcome Letter</a>
<br>
<a href="/pdf/Patient-Bill-of-Rights-and-Responsibilities.pdf">Patient Bill of Rights and Responsibilities</a>
<br>
<a href="/pdf/hippa.pdf">HIPAA Privacy Notice</a>
<br>
<a href="/pdf/Streamline-Billing.pdf">Streamline Billing</a>
<br>
<a href="/pdf/Insurance-Letter.pdf">Insurance Letter</a>
<br>
<a href="/pdf/Protection-Against-Medical-Identity-Theft.pdf">Protection Against Medical Identity Theft</a>
<br>
<a href="/pdf/Patient-Medications-and-Prescription-Refill-Policy.pdf">Patient Medications and Prescription Refill Policy</a>