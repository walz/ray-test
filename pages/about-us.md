---
layout: page
title: About Us
permalink: /about-us/
banner: about-banner.jpg
---
<h3>AALFA is an acronym for "All Ages Like Firsthand Attention"</h3>

​​An independent clinic since 1996, AALFA belongs to the Minnesota Healthcare Network (MHN), the largest organization of independent clinics in the five state area. We continue to grow substantially thanks to the caring environment the clinic provides.

Families looking for quality family healthcare will find what they are looking for at AALFA Family Clinic in White Bear Lake, MN. We provide you the best pro-life, Christian healthcare in the Twin Cities.

In addition to Family Practice and Internal Medicine, we are happy to provide the full range of gynecologic and obstetrical services including various surgical procedures and high-risk prenatal care.

<h3>Billing and Insurance:</h3>

We accept multiple insurance plans. It is the patient's responsibility to verify coverage and benefits with their insurance company prior to their appointment. The patient accepts ultimate responsibility for payment. Visit our Billing & Insurance page for more information.

<h3>Focus:</h3>
<ul>
<li>Chronic disease management</li>
<li>Infertility and recurring miscarriage</li>
<li>Sports Medicine</li>
</ul>

<h3>Hospital Affiliations:</h3>
<p><a href="http://www.healtheast.org/st-johns-hospital/about/about.html" target="_blank">​St. John's Hospital</a><br><a href="http://www.healtheast.org/st-josephs-hospital/about/about.html" target="_blank">St. Joseph's Hospital</a><br><a href="http://www.allinahealth.org/United-Hospital/" target="_blank">United Hospital</a><br><a href="https://www.childrensmn.org/AboutUs/Locations/SaintPaul/" target="_blank">Children's Hospital, St. Paul</a><br></p>
<p>Note: Dr. Matthew Anderson, OB/GYN is not affiliated with United or Children's Hospital.</p>

<h3>Specialties:</h3>

<ul><li>Family Medicine, including pediatrics and obstetrics</li>
    <li>Internal Medicine</li>
    <li>Obstetrics and Gynecology, including high-risk prenatal care​</li></ul>

<h3>Support Staff & Other Services</h3>

<p><a href="health-care-home--hch----medical-home--.html" target="">Care Coordination Program</a>; Jeanne Poliachik, RN - Care Coordinator<br><a href="diabetes-care.html" target="">Diabetes Lifestyle Classes</a><br><a href="fertility-care.html" target="">Natural Family Planning education</a><br>X-Ray and OB/GYN Ultrasound services</p>